[build-system]
requires = ["setuptools", "wheel"]
build-backend = "setuptools.build_meta"

[project]
name = "codon_harmonizer"
version = "0.0.0"
description = "Codon Harmonizer"
readme = "README.md"
authors = [
    {name = "Xavar", email="chaytonzquaranto@live.com"}
]
classifiers = [
    "Private :: Do Not Upload"
]
requires-python = ">=3.11"

dependencies = [
    "requests",
    "pip-tools>=7.3.0"
]

[project.optional-dependencies]
dev = [
    "pre-commit",
    "mypy"
]
test = [
    "pytest",
    "pytest-cov"
]
docs = [
    "sphinx",
    "sphinx-rtd-theme",
    "docutils",
    "myst-parser"
]

[tool.black]
line-length = 100
target-version = ["py39"]

[tool.ruff]
line-length = 100 # Must agree with Black
target-version = "py39"

select = [
    "B",   # flake8-bugbear
    "C4",  # flake8-comprehensions
    "D",   # pydocstyle
    "E",   # Error
    "F",   # pyflakes
    "I",   # isort
    "ISC", # flake8-implicit-str-concat
    "N",   # pep8-naming
    "PGH", # pygrep-hooks
    "PT",  # flake8-pytest-style
    "PTH", # flake8-use-pathlib
    "Q",   # flake8-quotes
    "S",   # bandit
    "SIM", # flake8-simplify
    "TRY", # tryceratops
    "UP",  # pyupgrade
    "W",   # Warning
    "YTT", # flake8-2020
]

exclude = [
    "__pycache__",
    "docs",
    "env",
    ".env",
    "venv",
    ".venv",
]

ignore = [
    "B905",  # zip strict=True; remove once python <3.10 support is dropped.
    "D100",
    "D101",
    "D102",
    "D103",
    "D104",
    "D105",
    "D106",
    "D107",
    "D200",
    "D401",
    "E402",
    "E501",
    "F401",
    "TRY003", # Avoid specifying messages outside exception class; overly strict, especially for ValueError
    "D205",
    "TRY002",
    "B008"
]

[tool.ruff.mccabe]
max-complexity = 18

[tool.ruff.flake8-bugbear]
extend-immutable-calls = [
    "chr",
    "typer.Argument",
    "typer.Option",
]

[tool.ruff.pydocstyle]
convention = "numpy"

[tool.ruff.per-file-ignores]
"codon_harmonizer/tests/*.py" = [
    "D100",
    "D101",
    "D102",
    "D103",
    "D104",
    "D105",
    "D106",
    "D107",
    "S101",   # use of "assert"
    "S102",   # use of "exec"
    "S106",   # possible hardcoded password.
    "PGH001", # use of "eval"
]

[tool.ruff.pep8-naming]
staticmethod-decorators = [
    "pydantic.validator",
    "pydantic.root_validator",
]
