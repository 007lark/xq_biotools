from codon_harmonizer.core import hello_world


class TestHello:
    def test_hello(self):
        assert hello_world() == "Hello world!"
