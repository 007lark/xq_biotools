FROM python:3.11

LABEL maintainer "Xavaar <chaytonzquaranto@live.com>"

ENV APP_HOME /home/app
WORKDIR $APP_HOME

COPY --chown=app:app requirements.txt $APP_HOME/
RUN pip3 install --no-cache-dir -r requirements.txt

COPY --chown=app:app . $APP_HOME

CMD ["python3"]
